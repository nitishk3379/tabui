package tab.com.tabui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashActivity extends Activity {

    private int splash_time = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        Thread splashBgThread = new Thread() {
            @SuppressWarnings("unchecked")
            public void run() {

                try {
                    sleep(splash_time * 1000);
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        splashBgThread.start();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

}
